/**
 * sindri-sql-pool.js
 *
 * Gerencia Conexão e Poll
 * Use esta classe quando desejar ter apenas uma conexão na aplicação
 *
 * Created by André Timermann on 29/10/2015
 *
 */
'use strict'


var config = require('config');
var logger = require('sindri-logger');
var util = require('util')


var pool = require('odbc-pool');

//TODO: PARAMETRIZAR
var odbcPool = new pool({
    min: 0,
    max: 10,
    log: false
});

module.exports = class ConnOdbc {

    /**
     * [constructor description]
     * @param  {[type]} connectionName [description]
     * @return {[type]}                [description]
     */
    constructor(connectionName) {

        var connectionConfig = config.get("database." + connectionName);

        // TODO: Por enquanto somente suporte a DB2 iseries, adicionar com o tempo
        this.connectionString = util.format(
            "DRIVER={%s};SCHEMA={%s};SYSTEM=%s;UID=%s;PWD=%s;PROTOCOL=TCPIP",
            connectionConfig.driver,
            connectionConfig.database,
            connectionConfig.host,
            connectionConfig.user,
            connectionConfig.password
        );

    }

    /**
     * Carrega POOL
     *
     * @return {[type]} [description]
     */
    openPool() {

        var self = this

        ////////////////////////////////////////////////////////////////////////////////
        // Promessa
        ////////////////////////////////////////////////////////////////////////////////
        return new Promise(function(resolve, reject) {

            logger.debug('Obtendo Pool...')

            var hasPool = odbcPool.open(self.connectionString, function(err, client) {

                if (err) {

                    console.log(err)
                    logger.error(err)
                    logger.info('Não foi possível conectar, tentando novamente...')
                    return self.openPool().then(function(client) {

                        resolve(client)

                    })

                } else {


                    logger.debug('Conexão realizada com sucesso...')
                    resolve(client)

                }

            });

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            /// Não disponível, tenta novamente
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (hasPool === false) {

                logger.debug("Recurso não disponível.")

            }

        })


    }


    /**
     * Executa uma consulta retornando Objeto com Row e Client
     *
     *
     * @param  {[type]} sqlQuery          [description]
     * @param  {[type]} bindingParameters [description]
     * @return {[type]}                   [description]
     */
    query(sqlQuery, bindingParameters) {

        var self = this;

        if (logger.is('debug')) {
            var startTime = new Date().getTime();
            console.log('---------- odbc query ----------')
            console.log("Query:", sqlQuery)
            if (bindingParameters) {
                console.log('-------------------------------')
                console.log("Data:", bindingParameters)
            }
            console.log('-------------------------------')
        }

        return this.openPool(this.connectionName)

        .then(function(client) {

            logger.debug('Executando consulta...')

            return new Promise(function(resolve, reject) {

                var queryFinished = false;

                ///////////////////////////////////////////////////////////////////////////////
                // Análise Se TIMOUT
                //////////////////////////////////////////////////////////////////////////////////

                setTimeout(function() {

                    logger.info('Timeout...')
                    if (!queryFinished) {
                        queryFinished = true;

                        client.close(function() {
                            logger.debug('Conexão Fechada')

                        })

                        self.closePool()
                        reject(new Error('Conexão Expirada, TIMEOUT'))
                    }

                    //TODO: Parametrizar
                }, 60000)

                /////////////////////////////////////////////////////////////////////////////////
                // Consulta
                ////////////////////////////////////////////////////////////////////////////////////
                client.query(sqlQuery, bindingParameters, function(error, rows) {

                    if (!queryFinished) {
                        queryFinished = true;

                        logger.debug('Fechando conexao...')
                            // Vamos fechar o recurso, já foi usado
                        client.close(function() {
                            logger.debug('Conexão Fechada')
                        })

                        if (error) {

                            console.log('ERROR MARK 3 ----------------------------------------------------------------');
                            console.log(error);

                            console.error(error)
                            reject(error)

                        } else {

                            if (logger.is('debug')) {

                                var endTime = new Date().getTime();
                                var time = endTime - startTime;

                                console.log('---------- response -----------')
                                console.log(rows)
                                console.log('-------------------------------')
                                console.log(' duration: ' + time + 'ms')
                                console.log('---------- completed ----------')
                            }

                            resolve(rows)
                        }

                    } else {
                        logger.debug('Conexão Expirou')
                    }
                })
            })

        })
    }

    closePool() {

        logger.info("Fechando Pool")
        odbcPool.close()

    }
}
