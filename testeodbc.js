require('colors')

odbcSimple()


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MSSQL
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// var mssqlPool = require('./apps/autoitManager/sindri-mssql-pool')
//
//
// console.log('A:' + mssqlPool.getConnectionStatus('tcp_mssql'))
// mssqlPool.getConnection('tcp_mssql').then(function(connection){
//
//     console.log('------------ OK -------------');
//
//     console.log('B:' + mssqlPool.getConnectionStatus('tcp_mssql'))
//     mssqlPool.closeConnection('tcp_mssql')
//     console.log('C:' + mssqlPool.getConnectionStatus('tcp_mssql'))
//
// })


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ODBC PRE REGISTRADO
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function odbcSimple() {
    var ConnOdbc = require('./apps/autoitManager/sindri-odbc-pool')
    var conn = new ConnOdbc('tcp_db2')

    setInterval(function() {

        console.log('Iniciando Ciclo'.red.bold)
        /*

        // PARA FECHAR TODAS AS CONEXÔES
        Para fechar a conexão vc precisa usar queryClient pra retornar o cliente e com isso e usar o conn.closepool como exemplo abaixo

        caso não queira se preocupar em fechar conexão, use apenas conn.query as conexões no pool serão fechadas automaticamente depois de um tempo

         */

        conn.query("SELECT CONTA, CONTN, CONTD FROM ARGYARBASF.CONTF WHERE ES <> ? FETCH FIRST 2000 ROWS ONLY", ['L'])
            .then(function(rows) {

                console.log('A');
                console.log(rows.length)

                //data.client.close()

                //conn.closePool()

            }).catch(function(err) {

                console.log(err);

            })


        conn.query("SELECT CONTA, CONTN, CONTD FROM ARGYARBASF.CONTF WHERE ES <> ? FETCH FIRST 1000 ROWS ONLY", ['L'])
            .then(function(rows) {

                console.log('B');
                console.log(rows.length)


            }).catch(function(err) {

                console.log(err);

            })



        conn.query("SELECT CONTA, CONTN, CONTD FROM ARGYARBASF.CONTF WHERE ES <> ? FETCH FIRST 1 ROWS ONLY", ['L'])
            .then(function(rows) {

                console.log('C');
                console.log(rows.length)



            }).catch(function(err) {

                console.log(err);

            })

    }, 10000);
}
